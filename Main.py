import Blockbuster as b
import Customer as c
import Movie as m

def quick_tests(bb):
    andrew = c.Customer("Andrew")
    jim = c.Customer("Jim")

    bb.add_customer(andrew)
    bb.add_customer(jim)



    movie1 = m.Movie("Movie 1","director 1","1")
    movie2 = m.Movie("Movie 2","director 2","2")
    movie3 = m.Movie("Movie 3", "director 3", 3)

    bb.add_movie(movie2)
    bb.add_movie(movie1)
    bb.add_movie(movie3)

    cust = bb.find_customer_by_id(1)
    if cust is not None:

        mv = bb.find_movie('Movie 1')
        if len(mv)>0:
            print(cust.get_name()+' rents: '+mv[0].get_title())
            bb.rent_movie(cust,mv[0])
            bb.show_rentals()
        else:
            print('movie not found')

    cust = bb.find_customer_by_id(1)
    if cust is not None:

        mv = bb.find_movie('Movie 3')
        if len(mv) > 0:
            print(cust.get_name() + ' rents: ' + mv[0].get_title())
            bb.rent_movie(cust, mv[0])
            bb.show_rentals()
        else:
            print('movie not found')

    cust = bb.find_customer_by_id(1)
    if cust is not None:

        mv = bb.find_movie('Movie 3')
        if len(mv) > 0:
            print(cust.get_name() + ' returns: ' + mv[0].get_title())
            bb.return_movie(cust, mv[0])
            bb.show_rentals()
        else:
            print('movie not found')

    bb.return_all_movies()
    print('return all')
    bb.show_rentals()

    cust = bb.find_customer_by_name('Jim')
    if cust is not None:

        mv = bb.find_movie('Movie 3')
        if len(mv) > 0:
            print(cust.get_name() + ' renets: ' + mv[0].get_title())
            bb.rent_movie(cust, mv[0])
            bb.show_rentals()
        else:
            print('movie not found')


def main():
    bb = b.Blockbuster.get()

    quick_tests(bb)

main()