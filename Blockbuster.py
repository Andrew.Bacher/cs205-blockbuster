import Rental

#Blockbuster contains movies that customers can checkout given certain conditions are met

class Blockbuster:
    s_blockbuster = None

    @classmethod
    def get(self):
        if self.s_blockbuster is None:
            self.s_blockbuster = Blockbuster()
        return self.s_blockbuster

    def __init__(self):
        self.movies = set()
        self.customers = set()
        self.rentals = set()

    def add_customer(self, customer):
        self.customers.add(customer)

    def get_customers(self):
        return self.customers

    def add_movie(self,movie):
        self.movies.add(movie)

    def get_movies(self):
        return self.movies

    def get_rentals(self):
        return self.rentals

    def find_customer_by_id(self,id):
        for c in self.customers:
            if c.get_id() == id:
                return c
        print('Customer with id: '+str(id)+' not found')
        return None

    def find_customer_by_name(self,name):
        for c in self.customers:
            if c.get_name() == name:
                return c
        print("Customer with name: "+name+" not found")
        return None

    def find_movie(self,title):
        movies = []
        for m in self.movies:
            if m.get_title() == title:
                movies.append(m)
        if(len(movies)==0):
            print("movie not found")

        return movies

    #INCORRECT IMPLEMENTATION
    def rent_movie_bad(self,customer,movie):
        if not self.is_rented(movie):
            rental = Rental.Rental(customer,movie)
            #customer.rent(movie) WE DON'T ADD THE MOVIE TO THE CUSTOMERS RENTAL SET
            self.rentals.add(rental)
            return rental
        else:
            return None

    #CORRECT IMPLEMENTATION
    def rent_movie(self,customer,movie):
        if not self.is_rented(movie):
            rental = Rental.Rental(customer,movie)
            customer.rent(movie)
            self.rentals.add(rental)
            return rental
        else:
            return None

    def is_rented(self,movie):
        for r in self.rentals:
            if r.get_movie() == movie:
                return True

        return False

    def show_rentals(self):
        for r in self.rentals:
            string = r.get_customer().to_string()+' => '+r.get_movie().to_string()
            print(string)

    def return_movie(self,customer,movie):
        for r in self.rentals:
            if r.get_customer() == customer and r.get_movie() == movie:
                self.rentals.remove(r)
                customer.return_movie(movie)
                return True
        return False

    def return_all_movies(self):
        for c in self.customers:
            c.rented_movies = set()
        self.rentals = set()
