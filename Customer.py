# A customer can rent movies from Blockbuster
# Movies checked out by a customer are saved in a set that customer owns

class Customer:
    id: int
    s_next_id = 0

    def __init__(self,name):
        self.name = name
        Customer.s_next_id = Customer.s_next_id + 1
        self.id = Customer.s_next_id
        self.rented_movies = set()

    def to_string(self):
        string = self.name+' id = '+str(self.id) + '; #rentals = '+str(len(self.rented_movies))
        return string

    def get_id(self):
        return self.id

    def get_name(self):
        return self.name

    def __eq__(self, other):
        return self.id == other.id

    def __hash__(self):
        return hash((self.name,self.id))

    def rent(self, movie):
        self.rented_movies.add(movie)

    def get_rentals(self):
        return self.rented_movies

    def return_movie(self,movie):
        for m in self.rented_movies:
            if m == movie:
                self.rented_movies.remove(movie)
                return True
        print("Movie not found: "+movie)
        return False