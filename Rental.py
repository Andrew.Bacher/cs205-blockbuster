# Customers rent movies from blockbuster
# when a customer rents a movie, an instance of rental is created


class Rental:
    def __init__(self,customer,movie):
        self.customer = customer
        self.movie = movie

    def get_movie(self):
        return self.movie

    def get_customer(self):
        return self.customer
