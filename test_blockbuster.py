import unittest
import Blockbuster as b
import Customer as c
import Movie as m


class TestBlockbuster(unittest.TestCase):
    bb = None

    @classmethod
    def setUpClass(cls):
        print("setting up")
        cls.bb = b.Blockbuster().get()

        #some customers and movies

        cls.andrew = c.Customer("Andrew")
        cls.jim = c.Customer("Jim")

        cls.movie1 = m.Movie("Movie 1", "director 1", "1")
        cls.movie2 = m.Movie("Movie 2", "director 2", "2")
        cls.movie3 = m.Movie("Movie 3", "director 3", 3)

        #add customers and movies to blockbuster object

        cls.bb.add_movie(cls.movie2)
        cls.bb.add_movie(cls.movie1)
        cls.bb.add_movie(cls.movie3)

        cls.bb.add_customer(cls.andrew)
        cls.bb.add_customer(cls.jim)

    def tearDown(self):
        print('tearing down')
        self.bb.return_all_movies()

####################################################################################################################

    def test_rent_incorrect(self):
        rental = self.bb.rent_movie_bad(self.jim,self.movie2)

        #the rental movie is movie2

        self.assertEqual(rental.get_movie(),self.movie2)

        #the movie is rented to Jim

        self.assertEqual(rental.get_customer(),self.jim)

        movies = self.jim.get_rentals()

        #Jim should have one movie in his rental set
        #But he does not since this is the incorrect implementation

        self.assertEqual(len(movies),1)

####################################################################################################################

    def test_rent_correct(self):
        rental = self.bb.rent_movie(self.andrew, self.movie1)

        #jim tries to rent a movie that is already rented
        #rental2 should be None

        rental2 = self.bb.rent_movie(self.jim, self.movie1)
        self.assertEqual(rental2,None)

        #there should only be one movie rented out

        rentals = self.bb.get_rentals()
        self.assertEqual(len(rentals),1)

        # the rental movie is movie1
        self.assertEqual(rental.get_movie(), self.movie1)

        # the movie is rented to Andrew
        self.assertEqual(rental.get_customer(), self.andrew)

        movies = self.andrew.get_rentals()

        # Andrew should have one movie in his rental set
        # And he does since this is the correct implementation

        self.assertEqual(len(movies), 1)

####################################################################################################################

    def test_is_rented(self):
        self.bb.rent_movie(self.andrew, self.movie3)
        self.bb.rent_movie(self.jim, self.movie2)

        isrented = self.bb.is_rented(self.movie3)
        self.assertEqual(isrented,True)

        isrented = self.bb.is_rented(self.movie2)
        self.assertEqual(isrented, True)

        isrented = self.bb.is_rented(self.movie1)
        self.assertEqual(isrented, False)

####################################################################################################################

    def test_return(self):
        self.bb.rent_movie(self.andrew, self.movie3)
        self.bb.rent_movie(self.jim, self.movie2)
        self.bb.return_movie(self.andrew, self.movie3)

        rentals = self.bb.get_rentals()
        self.assertEqual(len(rentals),1)

        self.bb.return_movie(self.jim, self.movie2)
        self.assertEqual(len(rentals), 0)

        self.bb.rent_movie(self.andrew, self.movie3)
        self.bb.rent_movie(self.jim, self.movie2)
        self.bb.return_all_movies()

        rentals = self.bb.get_rentals()
        self.assertEqual(len(rentals),0)

####################################################################################################################

    def test_find_movie(self):

        movies = self.bb.find_movie(self.movie2.get_title())

        self.assertEqual(len(movies),1)

        movies = self.bb.find_movie("not a movie")

        self.assertEqual(len(movies),0)

####################################################################################################################

    def test_find_customer(self):

        customer = self.bb.find_customer_by_id(1)

        self.assertEqual(self.andrew,customer)

        customer = self.bb.find_customer_by_id(100)

        self.assertEqual(None, customer)

        customer = self.bb.find_customer_by_name(self.jim.get_name())

        self.assertEqual(self.jim, customer)

        customer = self.bb.find_customer_by_name("not a customer")

        self.assertEqual(None, customer)

####################################################################################################################

if __name__ == '__main__':
    unittest.main()