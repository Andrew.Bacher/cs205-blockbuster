#Movie: Blockbuster has instances of movies

class Movie:
    def __init__(self,title,director,number):
        self.title = title
        self.director = director
        self.number = number

    def to_string(self):
        string = str(self.number)+' "'+self.title+'" Directed by: '+self.director
        return string

    def get_title(self):
        return self.title

    def get_director(self):
        return self.director

    def get_number(self):
        return self.number

    def __eq__(self, other):
        return self.title == other.title and self.director == other.director

    def __hash__(self):
        return hash((self.title,self.director))